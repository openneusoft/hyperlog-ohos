
/*
The MIT License (MIT)

Copyright (c) 2015-2017 HyperTrack (http://hypertrack.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package com.hypertrack.hyperlog;

import com.hypertrack.hyperlog.utils.HLDateTimeUtility;
import com.hypertrack.hyperlog.utils.Utils;
import ohos.data.rdb.RawRdbPredicates;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Aman on 20/09/17.
 */

class DeviceLogTable {

    private static final String TAG = DeviceLogTable.class.getSimpleName();
    private static final int DEVICE_LOG_REQUEST_QUERY_LIMIT = 5000;

    private static final String TABLE_NAME = "device_logs";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_DEVICE_LOG = "device_log";

    private static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME
            + " ("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_DEVICE_LOG + " TEXT"
            + ");";

    static void onCreate(RdbStore db) {
        if (db == null) {
            return;
        }

        try {
            db.executeSql(DATABASE_CREATE);
        } catch (Exception e) {
            e.printStackTrace();
            HyperLog.e("HYPERLOG", "DeviceLogTable: Exception occurred while onCreate: " + e);
        }
    }

    static void onUpgrade(RdbStore db, int oldVersion, int newVersion) {
        if (db == null) {
            return;
        }

        try {
            db.executeSql("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);

            HyperLog.i("HYPERLOG", "DeviceLogTable onUpgrade called. Executing drop_table query to clear old logs.");
        } catch (Exception e) {
            e.printStackTrace();
            HyperLog.e("HYPERLOG", "DeviceLogTable: Exception occurred while onUpgrade: " + e);
        }
    }

    static long getCount(RdbStore db) {
        try {
            if (db == null) {
                return 0;
            }

            return db.count(TABLE_NAME, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            HyperLog.e("HYPERLOG", "DeviceLogTable: Exception occurred while getCount: " + e);
            return 0L;
        }
    }

    static int getDeviceLogBatchCount(RdbStore db) {
        try {
            if (db == null) {
                return 0;
            }

            return (int) Math.ceil(getCount(db) * 1.0f / DEVICE_LOG_REQUEST_QUERY_LIMIT);

        } catch (Exception e) {
            e.printStackTrace();
            HyperLog.e("HYPERLOG", "DeviceLogTable: Exception occurred while getDeviceLogBatchCount: " + e);
            return 0;
        }
    }

    static void addDeviceLog(RdbStore db, String deviceLog) {
        if (db == null || Utils.isEmpty(deviceLog)) {
            return;
        }

        ValuesBucket valuesBucket = new ValuesBucket();
        valuesBucket.putString(COLUMN_DEVICE_LOG, deviceLog);

        try {
            long res = db.insert(TABLE_NAME, valuesBucket);
        } catch (Exception e) {
            e.printStackTrace();
            HyperLog.e("HYPERLOG", "DeviceLogTable: Exception occurred while addDeviceLog: " + e);
        }
    }

    static void deleteDeviceLog(RdbStore db, List<DeviceLogModel> deviceLogList) {
        if (db == null)
            return;

        StringBuilder builder = new StringBuilder();
        for (DeviceLogModel deviceLog : deviceLogList) {
            if (deviceLog != null && deviceLog.getId() > 0) {
                builder.append(deviceLog.getId()).append(",");
            }
        }

        if (builder.length() == 0) {
            return;
        }

        try {
            String ids = builder.toString();
            ids = ids.substring(0, ids.length() - 1);

            String whereClause = COLUMN_ID +
                    " IN (" +
                    ids +
                    ")";

            db.delete(new RawRdbPredicates(TABLE_NAME, whereClause, null));
        } catch (Exception e) {
            e.printStackTrace();
            HyperLog.e("HYPERLOG", "DeviceLogTable: Exception occurred while deleteDeviceLog: " + e);
        }
    }

    static void deleteAllDeviceLogs(RdbStore db) {
        if (db == null) {
            return;
        }

        try {
            db.delete(new RawRdbPredicates(TABLE_NAME));
        } catch (Exception e) {
            e.printStackTrace();
            HyperLog.e("HYPERLOG", "DeviceLogTable: Exception occurred while deleteAllDeviceLogs: " + e);
        }
    }

    static List<DeviceLogModel> getDeviceLogs(RdbStore db, int batch) {
        if (db == null) {
            return null;
        }

        int count = getDeviceLogBatchCount(db);
        batch--;
        if (count <= 1 || batch < 0) {
            batch = 0;
        }

        ArrayList<DeviceLogModel> deviceLogList = null;

        String limit = "1 = 1 LIMIT " + String.valueOf(batch * DEVICE_LOG_REQUEST_QUERY_LIMIT) + ", " + String.valueOf(DEVICE_LOG_REQUEST_QUERY_LIMIT);
        RawRdbPredicates rawRdbPredicates = new RawRdbPredicates(TABLE_NAME, limit, null);

        ResultSet resultSet = db.query(rawRdbPredicates, new String[]{COLUMN_ID, COLUMN_DEVICE_LOG});

        if (resultSet == null || resultSet.isClosed()) {
            return null;
        }
        try {
            if (resultSet.goToFirstRow()) {
                deviceLogList = new ArrayList<>();
                do {
                    if (resultSet.isClosed()) {
                        break;
                    }

                    String deviceLogString = resultSet.getString(1);
                    if (!Utils.isEmpty(deviceLogString)) {
                        DeviceLogModel deviceLog = new DeviceLogModel(deviceLogString);

                        // Get RowId for DeviceLogModel
                        Integer rowId = Integer.valueOf(resultSet.getString(0));
                        deviceLog.setId(rowId != null ? rowId : 0);

                        deviceLogList.add(deviceLog);
                    }
                } while (resultSet.goToNextRow());
            }
        } catch (Exception e) {
            e.printStackTrace();
            HyperLog.e("HYPERLOG", "DeviceLogTable: Exception occurred while getDeviceLogs: " + e);
        } finally {
            resultSet.close();
        }

        return deviceLogList;
    }

    public static void clearOldLogs(RdbStore db, int expiryTimeInSeconds) {
        if (db == null) {
            return;
        }

        try {
            Calendar calendar = Calendar.getInstance();
            //Set the calendar time to older time.
            calendar.add(Calendar.SECOND, -expiryTimeInSeconds);

            String date = HLDateTimeUtility.getFormattedTime(calendar.getTime());

            db.delete(new RawRdbPredicates(TABLE_NAME, COLUMN_DEVICE_LOG + "<?", new String[]{date}));

        } catch (Exception e) {
            e.printStackTrace();
            HyperLog.e("HYPERLOG", "DeviceLogTable: Exception occurred while deleteAllDeviceLogs: " + e);
        }
    }
}