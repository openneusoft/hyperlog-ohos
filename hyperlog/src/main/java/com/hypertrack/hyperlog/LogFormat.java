/*
The MIT License (MIT)

Copyright (c) 2015-2017 HyperTrack (http://hypertrack.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package com.hypertrack.hyperlog;

import com.google.gson.Gson;
import com.hypertrack.hyperlog.utils.HLDateTimeUtility;
import com.hypertrack.hyperlog.utils.Utils;
import ohos.app.Context;
import ohos.data.distributed.common.KvManagerConfig;
import ohos.data.distributed.common.KvManagerFactory;
import ohos.hiviewdfx.HiLog;
import ohos.system.version.SystemVersion;

import java.io.Serializable;


/**
 * Created by Aman on 10/10/17.
 */

/**
 * This class can be overridden to customise the log message format.
 * <br>
 * An instance of LogFormat needs to be passed to the method
 * {@link HyperLog#setLogFormat(LogFormat)} as parameter.
 */
public class LogFormat implements Serializable {

    private final String deviceUUID;

    public LogFormat(Context context) {
        String uuid = KvManagerFactory.getInstance().createKvManager(new KvManagerConfig(context)).getLocalDeviceInfo().getId();
        deviceUUID = Utils.isEmpty(uuid) ? "UnknownUUID" : uuid;
    }

    /**
     * Implement this method to override the default log message format.
     *
     * @param logLevel The level of logcat logging that Parse should do.
     * @param message  Log message that need to be customized.
     * @return Formatted Log Message that will store in database.
     */
    String formatLogMessage(int logLevel, String tag, String message) {

        String timeStamp = HLDateTimeUtility.getCurrentTime();
        String senderName = "1.0";
        String osVersion = "HarmonyOS-" + SystemVersion.getVersion();
        String logLevelName = getLogLevelName(logLevel);
        return getFormattedLogMessage(logLevelName, tag, message, timeStamp, senderName, osVersion, deviceUUID);
    }

    public String getFormattedLogMessage(String logLevelName, String tag, String message, String timeStamp,
                                         String senderName, String osVersion, String deviceUUID) {
        LogData data = new LogData(logLevelName, tag, message, timeStamp, senderName, osVersion, deviceUUID);
        return new Gson().toJson(data);
    }

    private static String getLogLevelName(int messageLogLevel) {

        String logLevelName;
        switch (messageLogLevel) {
            case HiLog.LOG_APP:
                logLevelName = "LOG_APP";
                break;
            case HiLog.DEBUG:
                logLevelName = "DEBUG";
                break;
            case HiLog.INFO:
                logLevelName = "INFO";
                break;
            case HiLog.WARN:
                logLevelName = "WARN";
                break;
            case HiLog.ERROR:
                logLevelName = "ERROR";
                break;
            case HiLog.FATAL:
                logLevelName = "FATAL";
                break;
            default:
                logLevelName = "NONE";
        }

        return logLevelName;
    }

}
