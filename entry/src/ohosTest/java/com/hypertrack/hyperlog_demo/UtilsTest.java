package com.hypertrack.hyperlog_demo;

import com.hypertrack.hyperlog.DeviceLogModel;
import com.hypertrack.hyperlog.LogFormat;
import com.hypertrack.hyperlog.utils.Utils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class UtilsTest {

    @Test
    public void writeStringsToFile() {
        // 异常系
        File resFile = Utils.writeStringsToFile(null, null, null);
        Assert.assertNull(resFile);

        // 正常系1
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        List<String> data = new ArrayList<>();
        data.add("test1");
        data.add("test2");
        File resFile2 = Utils.writeStringsToFile(context, data, "testFile");
        Assert.assertNotNull(resFile2);
        Assert.assertNotNull("testFile", resFile2.getName());
    }

    @Test
    public void getDeviceId() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        String res = Utils.getDeviceId(context);
        Assert.assertNotNull(res);
        Assert.assertEquals(32, res.length());
    }

    @Test
    public void getByteData() {
        List<DeviceLogModel> deviceLogs = new ArrayList<>();
        deviceLogs.add(new DeviceLogModel("test1"));
        deviceLogs.add(new DeviceLogModel(1, "test2"));
        byte[] res = Utils.getByteData(deviceLogs);
        byte[] exp = new byte[]{116, 101, 115, 116, 49, 10, 116, 101, 115, 116, 50, 10};
        Assert.assertArrayEquals(exp, res);
    }

    @Test
    public void saveLogFormat() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Utils.saveLogFormat(context, null);
        LogFormat logFormat = Utils.getLogFormat(context);
        Assert.assertNotNull(logFormat);

        Utils.saveLogFormat(context, new LogFormat(context));
        LogFormat logFormat2 = Utils.getLogFormat(context);
        Assert.assertNotNull(logFormat2);
    }

    @Test
    public void getLogFormat() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        LogFormat logFormat = Utils.getLogFormat(context);
        Assert.assertNotNull(logFormat);

        Preferences preferences = new DatabaseHelper(context).getPreferences("com.hyperlog:Preferences");
        preferences.putString("com.hyperlog:LogFormat", "");
        preferences.flush();
        LogFormat logFormat2 = Utils.getLogFormat(context);
        Assert.assertNotNull(logFormat2);

        Utils.saveLogFormat(context, new LogFormat(context));
        LogFormat logFormat3 = Utils.getLogFormat(context);
        Assert.assertNotNull(logFormat3);
    }

    @Test
    public void isEmpty() {
        Assert.assertEquals(true, Utils.isEmpty(null));
        Assert.assertEquals(true, Utils.isEmpty(""));
        Assert.assertEquals(false, Utils.isEmpty("a"));
    }
}