package com.hypertrack.hyperlog_demo;

import com.hypertrack.hyperlog.HyperLog;
import com.hypertrack.hyperlog.LogFormat;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class HyperLogTest {

    @Test
    public void initialize() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CustomLogMessageFormat customLogMessageFormat = new CustomLogMessageFormat(context);

        HyperLog.initialize(context, customLogMessageFormat);

        HyperLog.initialize(context, customLogMessageFormat);

        HyperLog.initialize(context, null);

        HyperLog.initialize(context);

        HyperLog.initialize(context, 1);
    }

    @Test
    public void setLogFormat() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        HyperLog.initialize(context);

        CustomLogMessageFormat customLogMessageFormat = new CustomLogMessageFormat(context);
        HyperLog.setLogFormat(customLogMessageFormat);
    }

    @Test
    public void v() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CustomLogMessageFormat customLogMessageFormat = new CustomLogMessageFormat(context);
        HyperLog.initialize(context,customLogMessageFormat);
        HyperLog.setLogLevel(HiLog.LOG_APP);
        HyperLog.v(HyperLog.TAG_HYPERLOG, "HyperLog-V-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<String> list = HyperLog.getDeviceLogsAsStringList(false);
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void d() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        HyperLog.initialize(context);
        CustomLogMessageFormat customLogMessageFormat = new CustomLogMessageFormat(context);
        HyperLog.setLogFormat(customLogMessageFormat);

        HyperLog.setLogLevel(HiLog.LOG_APP);
        HyperLog.d(HyperLog.TAG_HYPERLOG, "HyperLog-D-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<String> list = HyperLog.getDeviceLogsAsStringList(false);
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void i() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        HyperLog.initialize(context);
        CustomLogMessageFormat customLogMessageFormat = new CustomLogMessageFormat(context);
        HyperLog.setLogFormat(customLogMessageFormat);

        HyperLog.setLogLevel(HiLog.LOG_APP);
        HyperLog.i(HyperLog.TAG_HYPERLOG, "HyperLog-I-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<String> list = HyperLog.getDeviceLogsAsStringList(false);
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void w() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        HyperLog.initialize(context);
        CustomLogMessageFormat customLogMessageFormat = new CustomLogMessageFormat(context);
        HyperLog.setLogFormat(customLogMessageFormat);

        HyperLog.setLogLevel(HiLog.LOG_APP);
        HyperLog.w(HyperLog.TAG_HYPERLOG, "HyperLog-W-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<String> list = HyperLog.getDeviceLogsAsStringList(false);
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void e() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        HyperLog.initialize(context);
        CustomLogMessageFormat customLogMessageFormat = new CustomLogMessageFormat(context);
        HyperLog.setLogFormat(customLogMessageFormat);

        HyperLog.setLogLevel(HiLog.LOG_APP);
        HyperLog.e(HyperLog.TAG_HYPERLOG, "HyperLog-E-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<String> list = HyperLog.getDeviceLogsAsStringList(false);
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void exception() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        HyperLog.initialize(context);
        CustomLogMessageFormat customLogMessageFormat = new CustomLogMessageFormat(context);
        HyperLog.setLogFormat(customLogMessageFormat);

        HyperLog.setLogLevel(HiLog.LOG_APP);
        HyperLog.exception(HyperLog.TAG_HYPERLOG, "HyperLog-Exception-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<String> list = HyperLog.getDeviceLogsAsStringList(false);
        Assert.assertEquals(1, list.size());

        Exception exception = null;
        HyperLog.exception(HyperLog.TAG_HYPERLOG, exception);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<String> list2 = HyperLog.getDeviceLogsAsStringList(false);
        Assert.assertEquals(1, list2.size());

        exception = new RuntimeException();
        HyperLog.exception(HyperLog.TAG_HYPERLOG, exception);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<String> list3 = HyperLog.getDeviceLogsAsStringList(false);
        Assert.assertEquals(2, list3.size());
    }

    @Test
    public void a() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        HyperLog.initialize(context);
        CustomLogMessageFormat customLogMessageFormat = new CustomLogMessageFormat(context);
        HyperLog.setLogFormat(customLogMessageFormat);

        HyperLog.setLogLevel(HiLog.LOG_APP);
        HyperLog.a("HyperLog-A-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<String> list = HyperLog.getDeviceLogsAsStringList(false);
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void getDeviceLogs() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        List res = HyperLog.getDeviceLogs();
        Assert.assertNull(res);

        HyperLog.initialize(context, new CustomLogMessageFormat(context));
        HyperLog.d(HyperLog.TAG_HYPERLOG, "HyperLog-D-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List res2 = HyperLog.getDeviceLogs();
        Assert.assertEquals(1, res2.size());

        List res3 = HyperLog.getDeviceLogs(false);
        Assert.assertNull(res3);
    }

    @Test
    public void getDeviceLogsAsStringList() throws Exception {
        List res = HyperLog.getDeviceLogsAsStringList();
        Assert.assertEquals(0, res.size());

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        HyperLog.initialize(context, new CustomLogMessageFormat(context));

        List res2 = HyperLog.getDeviceLogsAsStringList();
        Assert.assertEquals(0, res2.size());

        HyperLog.d(HyperLog.TAG_HYPERLOG, "HyperLog-D-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List res3 = HyperLog.getDeviceLogsAsStringList();
        Assert.assertEquals(1, res3.size());

        List res4 = HyperLog.getDeviceLogsAsStringList();
        Assert.assertEquals(0, res4.size());
    }

    @Test
    public void getDeviceLogsInFile() throws Exception {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();

        File resFile = HyperLog.getDeviceLogsInFile(context);
        Assert.assertNull(resFile);


        File resFile2 = HyperLog.getDeviceLogsInFile(context);
        Assert.assertNull(resFile2);

        HyperLog.initialize(context, new CustomLogMessageFormat(context));
        HyperLog.d(HyperLog.TAG_HYPERLOG, "HyperLog-D-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<String> list = HyperLog.getDeviceLogsAsStringList(false);
        Assert.assertEquals(1, list.size());
        File resFile3 = HyperLog.getDeviceLogsInFile(context, "testFile");
        Assert.assertNotNull(resFile3);
        Assert.assertEquals("testFile", resFile3.getName());
        List<String> list2 = HyperLog.getDeviceLogsAsStringList(false);
        Assert.assertEquals(0, list2.size());
    }

    @Test
    public void hasPendingDeviceLogs() throws Exception {
        boolean res = HyperLog.hasPendingDeviceLogs();
        Assert.assertEquals(false, res);

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        HyperLog.initialize(context, new LogFormat(context));
        boolean res2 = HyperLog.hasPendingDeviceLogs();
        Assert.assertEquals(false, res2);

        HyperLog.d(HyperLog.TAG_HYPERLOG, "HyperLog-D-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean res3 = HyperLog.hasPendingDeviceLogs();
        Assert.assertEquals(true, res3);
    }

    @Test
    public void getDeviceLogsCount() throws Exception {
        long res = HyperLog.getDeviceLogsCount();
        Assert.assertEquals(0, res);

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        HyperLog.initialize(context, new LogFormat(context));
        HyperLog.d(HyperLog.TAG_HYPERLOG, "HyperLog-D-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long res2 = HyperLog.getDeviceLogsCount();
        Assert.assertEquals(1, res2);
    }

    @Test
    public void getDeviceLogBatchCount() throws Exception {
        long res = HyperLog.getDeviceLogBatchCount();
        Assert.assertEquals(0, res);

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        HyperLog.initialize(context, new LogFormat(context));
        HyperLog.d(HyperLog.TAG_HYPERLOG, "HyperLog-D-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long res2 = HyperLog.getDeviceLogBatchCount();
        Assert.assertEquals(1, res2);
    }

    @Test
    public void pushLogs() {
    }

    @Test
    public void deleteLogs() throws Exception {
        HyperLog.deleteLogs();

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        HyperLog.initialize(context, new CustomLogMessageFormat(context));
        HyperLog.d(HyperLog.TAG_HYPERLOG, "HyperLog-D-Test");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<String> list = HyperLog.getDeviceLogsAsStringList(false);
        Assert.assertEquals(1, list.size());

        HyperLog.deleteLogs();
    }
}