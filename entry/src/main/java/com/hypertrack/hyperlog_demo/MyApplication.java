package com.hypertrack.hyperlog_demo;

import com.hypertrack.hyperlog.HyperLog;
import ohos.aafwk.ability.AbilityPackage;
import ohos.hiviewdfx.HiLog;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        HyperLog.initialize(this,new CustomLogMessageFormat(this));
        HyperLog.setLogLevel(HiLog.LOG_APP);
        HyperLog.setURL("<Set URL>");
    }
}
