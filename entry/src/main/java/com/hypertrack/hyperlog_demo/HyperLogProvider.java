package com.hypertrack.hyperlog_demo;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;

import java.util.List;

public class HyperLogProvider extends BaseItemProvider {

    private List<String> logsList;
    private Ability ability;

    public HyperLogProvider(Ability ability, List<String> logsList) {
        this.ability = ability;
        this.logsList = logsList;
    }

    // 用于保存列表项中的子组件信息
    public class LogsHolder {
        Text logText;

        LogsHolder(Component component) {
            logText = (Text) component.findComponentById(ResourceTable.Id_log);
        }
    }

    @Override
    public int getCount() {
        return logsList == null ? 0 : logsList.size();
    }

    @Override
    public Object getItem(int position) {
        if (logsList != null && position >= 0 && position < logsList.size()) {
            return logsList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        LogsHolder logsHolder;
        String log = logsList.get(position);
        if (component == null) {
            cpt = LayoutScatter.getInstance(ability).parse(ResourceTable.Layout_layout_item_logs, null, false);
            logsHolder = new LogsHolder(cpt);
            // 将获取到的子组件信息绑定到列表项的实例中
            cpt.setTag(logsHolder);
        } else {
            cpt = component;
            // 从缓存中获取到列表项实例后，直接使用绑定的子组件信息进行数据填充。
            logsHolder = (LogsHolder) cpt.getTag();
        }
        logsHolder.logText.setText(log);
        return cpt;
    }
}
