package com.hypertrack.hyperlog_demo;

import com.hypertrack.hyperlog.HLCallback;
import com.hypertrack.hyperlog.HyperLog;
import com.hypertrack.hyperlog.error.HLErrorResponse;
import com.hypertrack.hyperlog.utils.Utils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

public class MainAbility extends Ability {

    private static final String TAG = MainAbility.class.getSimpleName();
    private static final int domain = 0x0;
    private static final HiLogLabel debugLabel = new HiLogLabel(HiLog.DEBUG, domain, TAG);
    private static final HiLogLabel errorLabel = new HiLogLabel(HiLog.ERROR, domain, TAG);

    TextField logText, logLevel, endPointUrl;
    Button setLogLevelBtn, setUrlBtn, addLogBtn, showLogsBtn, getFileBtn, deleteBtn, nextLogBtn, pushLogBtn, addLogQueryLimitBtn;
    Picker log_level_picker;
    ListContainer listContainer;
    List<String> logsList = new ArrayList<>();
    HyperLogProvider hyperLogProvider;
    int batchNumber = 1;
    int count = 0;
    String[] logs = new String[]{"Download Library", "Library Downloaded", "Initialize Library", "Library Initialized", "Log Message", "Message Logged",
            "Create Log File", "Log File Created", "Push Logs to Server", "Logs Pushed to Server", "Logs Deleted", "Library Downloaded", "Library Initialized", "Message Logged",
            "Log File Created", "Logs Pushed to Server", "Logs Deleted"};
    ToastDialog toastDialog;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        //Set Custom Log Message Format.
        HyperLog.setLogFormat(new CustomLogMessageFormat(this));
        endPointUrl = (TextField) findComponentById(ResourceTable.Id_end_point_url);
        logLevel = (TextField) findComponentById(ResourceTable.Id_log_level);
        logText = (TextField) findComponentById(ResourceTable.Id_logText);
        setUrlBtn = (Button) findComponentById(ResourceTable.Id_set_url);
        setLogLevelBtn = (Button) findComponentById(ResourceTable.Id_set_log_level);
        addLogBtn = (Button) findComponentById(ResourceTable.Id_add_log);
        showLogsBtn = (Button) findComponentById(ResourceTable.Id_show_logs);
        getFileBtn = (Button) findComponentById(ResourceTable.Id_get_file);
        deleteBtn = (Button) findComponentById(ResourceTable.Id_delete);
        nextLogBtn = (Button) findComponentById(ResourceTable.Id_next_log);
        pushLogBtn = (Button) findComponentById(ResourceTable.Id_push_log);
        addLogQueryLimitBtn = (Button) findComponentById(ResourceTable.Id_add_log_query_limit);
        log_level_picker = (Picker) findComponentById(ResourceTable.Id_log_level_picker);
        log_level_picker.setMinValue(0);
        log_level_picker.setMaxValue(6);
        log_level_picker.setValue(0);
        log_level_picker.setDisplayedData(new String[]{"Level: 0 - LOG_APP", "Level: 3 - DEBUG", "Level: 4 - INFO", "Level: 5 - WARN", "Level: 6 - ERROR", "Level: 6 - EXCEPTION", "Level: 7 - FATAL"});
        log_level_picker.setWheelModeEnabled(true);
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_listContainer);
        hyperLogProvider = new HyperLogProvider(this, logsList);
        listContainer.setItemProvider(hyperLogProvider);
        setLogLevelBtn.setClickedListener(this::setLogLevel);
        setUrlBtn.setClickedListener(this::setEndPoint);
        addLogBtn.setClickedListener(this::addLog);
        showLogsBtn.setClickedListener(this::showLogs);
        getFileBtn.setClickedListener(this::getFile);
        deleteBtn.setClickedListener(this::deleteLogs);
        nextLogBtn.setClickedListener(this::nextLog);
        pushLogBtn.setClickedListener(this::pushLog);
        addLogQueryLimitBtn.setClickedListener(this::addLogQueryLimit);
        listContainer.setLongClickable(false);
    }

    public void setLogLevel(Component view) {
        String input = logLevel.getText();
        if (Utils.isEmpty(input)) {
            showToast("Please enter log level.");
            return;
        }
        if (!input.matches("[0-7]?")) {
            showToast("The log level is 0-7.");
            return;
        }
        HyperLog.setLogLevel(Integer.parseInt(logLevel.getText()));
        showToast("Log level is set to " + input + ".");
    }

    public void setEndPoint(Component view) {
        String url = endPointUrl.getText().toString();
        if (Utils.isEmpty(url)) {
            new ToastDialog(this).setText("Url can't be empty.").setDuration(2000).show();
            return;
        }
        HyperLog.setURL(url);
    }

    public void showLogs(Component component) {
        logsList.clear();
        List<String> logList = HyperLog.getDeviceLogsAsStringList(false);
        for (int i = 0; i < logList.size(); i++) {
            if (i > 99) {
                continue;
            }
            logsList.add(logList.get(i));
        }
        hyperLogProvider.notifyDataChanged();
        batchNumber = 1;
        showToast("Show logs top 100");
    }

    public void addLog(Component component) {
        if (!Utils.isEmpty(logText.getText())) {
            hyperLogOut(log_level_picker.getValue(), TAG, logText.getText());

            logText.setText("");
            if (count >= logs.length) {
                count = 0;
            }
            logText.setText(logs[count++]);

            showToast("Log Added");
            showLogs(component);
        }
    }

    public void addLogQueryLimit(Component component) {
        if (!Utils.isEmpty(logText.getText())) {
            final String log = logText.getText();
            new Thread(() -> {
                for (int i = 1; i <= 5001; i++) {
                    hyperLogOut(log_level_picker.getValue(), TAG, log + i);
                }
            }).start();
            logText.setText("");
            if (count >= logs.length) {
                count = 0;
            }
            logText.setText(logs[count++]);

            showToast("Log Added 5001");
        }
    }

    public void getFile(Component component) {
        File file = HyperLog.getDeviceLogsInFile(this, false);
        if (file != null && file.exists())
            showToast("File Created at: " + file.getAbsolutePath());
    }

    public void deleteLogs(Component component) {
        HyperLog.deleteLogs();
        showToast("Logs deleted");
        logsList.clear();
        hyperLogProvider.notifyDataChanged();
    }

    public void nextLog(Component component) {
        logsList.clear();
        List<String> logList = HyperLog.getDeviceLogsAsStringList(false, ++batchNumber);
        for (int i = 0; i < logList.size(); i++) {
            if (i > 99) {
                continue;
            }
            logsList.add(logList.get(i));
        }
        hyperLogProvider.notifyDataChanged();
        showToast("NextLogs show top 100");
    }

    public void pushLog(Component component) {

        if (Utils.isEmpty(HyperLog.getURL())) {
            showToast("Set EndPoint URL First");
            //new ToastDialog(this).setText("Set EndPoint URL First").setDuration(2000).show();
            endPointUrl.requestFocus();
            return;
        }
        //Extra header to post request
        HashMap<String, String> params = new HashMap<>();
        params.put("timezone", TimeZone.getDefault().getID());

        HyperLog.pushLogs(this, params, true, new HLCallback() {
            @Override
            public void onSuccess(Object response) {
                showToast("Log Pushed");
                HiLog.debug(debugLabel, "onSuccess: " + response);
                logsList.clear();
                hyperLogProvider.notifyDataChanged();
            }

            @Override
            public void onError(HLErrorResponse HLErrorResponse) {
                showToast("Log Push Error");
                HiLog.error(errorLabel, "onError: " + HLErrorResponse.getErrorMessage());
            }
        });
    }

    private void showToast(String message) {
        if (toastDialog != null)
            toastDialog.cancel();
        toastDialog = new ToastDialog(this).setText(message).setDuration(2000);
        toastDialog.show();
    }

    private void hyperLogOut(int logLevel, String tag, String message) {
        switch (logLevel) {
            case 0:
                HyperLog.v(tag, message);
                break;
            case 1:
                HyperLog.d(tag, message);
                break;
            case 2:
                HyperLog.i(tag, message);
                break;
            case 3:
                HyperLog.w(tag, message);
                break;
            case 4:
                HyperLog.e(tag, message);
                break;
            case 5:
                HyperLog.exception(tag, message);
                break;
            case 6:
                HyperLog.a(message);
                break;
        }
    }
}
