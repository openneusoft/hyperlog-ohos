# hyperlog-ohos

**本项目是基于开源项目hyperlog-android进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/hypertrack/hyperlog-android ）追踪到原项目版本**

#### 项目介绍

- 项目名称：程序日志记录工具
- 所属系列：ohos的第三方组件适配移植
- 功能：实用的程序日志记录工具，用于将日志存储到数据库中。
- 项目移植状态：完成
- 调用差异：无
- 原项目Doc地址：https://github.com/hypertrack/hyperlog-android
- 原项目基线版本：v0.0.10 , sha1:e5aac74eb7d9f1d0c56152ff2792a075b63fd3a2
- 编程语言：Java 
- 外部库依赖：com.google.code.gson:gson:2.8.1

#### 效果展示

<img src="https://gitee.com/openneusoft/hyperlog-ohos/raw/master/preview/preview.gif"/>

#### 安装教程

方法1.

1. 编译har包hyperlog.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'io.github.dzsf:hyperlog-ohos:1.0.1'
}
```

#### 使用说明

1. 设置日志格式化模板

```
    HyperLog.setLogFormat(new CustomLogMessageFormat(this));
```

 

2. 设置日志输出控制台级别
```
    HyperLog.setLogLevel(Integer.parseInt(logLevel.getText()));
```



3. 调用方法即可将对应级别的log信息写入到本地数据库

```
    HyperLog.v(tag, message);
    HyperLog.d(tag, message);
    HyperLog.i(tag, message);
    HyperLog.w(tag, message);
    HyperLog.e(tag, message);
    HyperLog.exception(tag, message);
    HyperLog.a(message);
```

 

4. 调用该方法可获取第一组的日志信息，5000条日志为一组

```
    HyperLog.getDeviceLogsAsStringList(false);
```

 

5. 调用该方法可删除本地数据库的日志信息

```
    HyperLog.deleteLogs();
```

6. 默认日志格式化样式

```
timeStamp + " | " + appVersion + " : " + osVersion + " | " + deviceUUID + " | [" + logLevelName + "]: " + message
```
```
2017-10-05T14:46:36.541Z 1.0 | 0.0.1 : Hos API5 | 62bb1162466c3eed | [INFO]: Log has been pushed
```



#### 版本迭代

- v1.0.1

#### 版权和许可信息

```
MIT License

Copyright (c) 2018 HyperTrack

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```